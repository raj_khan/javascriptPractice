<!DOCTYPE html>
<html lang="en">
    <head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Calculator</title>

	<!-- Bootstrap -->
	<link href="asset/css/style.css" rel="stylesheet">
	<link href="asset/css/bootstrap.min.css" rel="stylesheet">

	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
    </head>
    <body>


	<div class="wrap">
	    <div class="game"></div>

	    <div class="modal-overlay">
		<div class="modal">
		    <h2 class="winner">You Rock!</h2>
		    <button class="restart">Play Again?</button>
		    <p class="message">Developed on <a href="https://codepen.io">CodePen</a> by <a href="https://codepen.io/natewiley">Nate Wiley</a></p>
		    <p class="share-text">Share it?</p>
		    <ul class="social">
			<li><a target="_blank" class="twitter" href="https://twitter.com/share?url=https://codepen.io/natewiley/pen/HBrbL"><span class="fa fa-twitter"></span></a></li>
			<li><a target="_blank" class="facebook" href="https://www.facebook.com/sharer.php?u=https://codepen.io/natewiley/pen/HBrbL"><span class="fa fa-facebook"></span></a></li>
			<li><a target="_blank" class="google" href="https://plus.google.com/share?url=https://codepen.io/natewiley/pen/HBrbL"><span class="fa fa-google"></span></a></li>
		    </ul>
		</div>
	    </div>
	    <footer>
		<p class="disclaimer">All logos are property of their respective owners, No Copyright infringement intended.</p>
	    </footer>
	</div><!-- End Wrap -->

	
	
	<!--Footer-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

	<script src="asset/js/bootstrap.min.js"></script>
	<script src="asset/js/main.js"></script>



    </body>
</html>