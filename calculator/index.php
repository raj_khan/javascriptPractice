<!DOCTYPE html>
<html lang="en">
    <head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Calculator</title>

	<!-- Bootstrap -->
	<link href="asset/css/style.css" rel="stylesheet">
	<link href="asset/css/bootstrap.min.css" rel="stylesheet">

	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
    </head>
    <body>


	<h1>JavaScript Calculator</h1>
	<p class="warning">Don't divide by zero</p>

	<div id="calculator" class="calculator">

	    <button id="clear" class="clear">C</button>

	    <div id="viewer" class="viewer">0</div>

	    <button class="num" data-num="7">7</button>
	    <button class="num" data-num="8">8</button>
	    <button class="num" data-num="9">9</button>
	    <button data-ops="plus" class="ops">+</button>

	    <button class="num" data-num="4">4</button>
	    <button class="num" data-num="5">5</button>
	    <button class="num" data-num="6">6</button>
	    <button data-ops="minus" class="ops">-</button>

	    <button class="num" data-num="1">1</button>
	    <button class="num" data-num="2">2</button>
	    <button class="num" data-num="3">3</button>
	    <button data-ops="times" class="ops">*</button>

	    <button class="num" data-num="0">0</button>
	    <button class="num" data-num=".">.</button>
	    <button id="equals" class="equals" data-result="">=</button>
	    <button data-ops="divided by" class="ops">/</button>
	</div>

	<button id="reset" class="reset">Reset Universe?</button>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	
	<script src="asset/js/bootstrap.min.js"></script>
	<script src="asset/js/main.js"></script>
	
	
	
    </body>
</html>