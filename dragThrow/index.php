<!DOCTYPE html>
<html lang="en">
    <head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Calculator</title>

	<!-- Bootstrap -->
	<link href="asset/css/style.css" rel="stylesheet">
	<link href="asset/css/bootstrap.min.css" rel="stylesheet">

	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
    </head>
    <body>


	<link href='https://fonts.googleapis.com/css?family=Asap:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Signika+Negative:300,400,700' rel='stylesheet' type='text/css'>
<div id="container">
		<div class="box" id="box1">Drag and throw me</div>
		<div class="box" id="box2" style="left:392px; background-color:red;">Drag and throw me too</div>
</div>
<div class="controls">
				<ul>
					<li class="controlsTitle">Options</li>
					<li>
						<label><input type="checkbox" name="snap" id="snap" value="1" /> Snap end position to grid</label>
					</li>
					<li>
						<label><input type="checkbox" name="liveSnap" id="liveSnap" value="1" /> Live snap</label>
					</li>
				</ul>
			</div>

	
	
	<!--Footer-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

	<script src="asset/js/bootstrap.min.js"></script>
	<script src="asset/js/main.js"></script>



    </body>
</html>