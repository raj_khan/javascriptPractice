<!DOCTYPE html>
<html lang="en">
    <head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Calculator</title>

	<!-- Bootstrap -->
	<link href="asset/css/style.css" rel="stylesheet">
	<link href="asset/css/bootstrap.min.css" rel="stylesheet">

	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
    </head>
    <body>


	<div>
	    <canvas></canvas>
	    <ul>
		<li><label>Width:</label><input id=width type="text"/></li>
		<li><label>Height:</label><input id=height type="text"/></li>
		<li><label>Path Width:</label><input id=pathwidth type="text"/></li>
		<li><label>Wall Width:</label><input id=wallwidth type="text"/></li>
		<li><label>Outer Width:</label><input id=outerwidth type="text"/></li>
		<li><label>Path Color:</label><input id=pathcolor type="text"/></li>
		<li><label>Wall Color:</label><input id=wallcolor type="text"/></li>
		<li><label>Seed:</label><input id=seed type="text"/></li>
		<li><input id=randomseed type="button" value="Random Seed"/></li>
	    </ul>
	</div>



	<!--Footer-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

	<script src="asset/js/bootstrap.min.js"></script>
	<script src="asset/js/main.js"></script>



    </body>
</html>