<!DOCTYPE html>
<html lang="en">
    <head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Tearable Cloth</title>

	<!-- Bootstrap -->

	<link href="asset/css/bootstrap.min.css" rel="stylesheet">
	<link href="asset/css/style.css" rel="stylesheet">

	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
    </head>
    <body>


	<canvas id="c"></canvas>

	<div id="new">Updated version <a href="https://codepen.io/dissimulate/details/eZxEBO/">here</a>.</div>

	<div id="info">
	    <div id="top">
		<a id="close" href="">&times;</a>
	    </div>
	    <p>
		<br>- Tear the cloth with your mouse.
		<br>
		<br>- Right click and drag to cut the cloth
		<br>
		<br>
		<br><a id="github" target="_blank" href="https://github.com/suffick/Tearable-Cloth">View on GitHub</a>
		<br>
	    </p>
	</div>


	<!--Footer-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

	<script src="asset/js/bootstrap.min.js"></script>
	<script src="asset/js/main.js"></script>



    </body>
</html>